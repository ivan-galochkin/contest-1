import java.io.*;
import java.util.*;

class MinQueue {
    Queue<Integer> q;
    Deque<Integer> dq;
    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out), 32000));

    public MinQueue() {
        q = new ArrayDeque<>();
        dq = new ArrayDeque<>();
    }

    void Size() {
        out.println(q.size());
        out.flush();
    }

    void Clear() {
        q = new ArrayDeque<>();
        dq = new ArrayDeque<>();
        out.println("ok");
        out.flush();
    }

    void Enqueue(int num) {
        while (!dq.isEmpty() && dq.getLast() > num) {
            dq.removeLast();
        }

        dq.addLast(num);
        q.add(num);
        out.println("ok");
        out.flush();
    }

    void Dequeue() {
        if (q.isEmpty()) {
            out.println("error");
            out.flush();
        } else {
            if (Objects.equals(dq.getFirst(), q.peek())) {
                dq.removeFirst();
            }
            out.println(q.remove());
            out.flush();
        }

    }

    void Min() {
        if (!q.isEmpty()) {
            out.println(dq.getFirst());
            out.flush();
        } else {
            out.println("error");
            out.flush();
        }
    }

    void Front() {
        if (q.isEmpty()) {
            out.println("error");
            out.flush();
        } else {
            out.println(q.peek());
            out.flush();
        }

    }
}

class Utils {
    public static void Process(MinQueue mq, BufferedReader in) throws IOException {
        String[] commands = in.readLine().split(" ");
        if (Objects.equals(commands[0], "enqueue")) {
            mq.Enqueue(Integer.parseInt(commands[1]));
        } else if (Objects.equals(commands[0], "dequeue")) {
            mq.Dequeue();
        } else if (Objects.equals(commands[0], "front")) {
            mq.Front();
        } else if (Objects.equals(commands[0], "size")) {
            mq.Size();
        } else if (Objects.equals(commands[0], "clear")) {
            mq.Clear();
        } else if (Objects.equals(commands[0], "min")) {
            mq.Min();
        }
    }
}

class Main {
    public static void main(String[] args) throws IOException {
        var mq = new MinQueue();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in), 32000);
        int n = Integer.parseInt(in.readLine());

        for (int i = 0; i < n; ++i) {
            Utils.Process(mq, in);
        }
    }
}
