#include <deque>
#include <iostream>

void Balance(std::deque<int> &deque1, std::deque<int> &deque2) {
  if (deque2.size() > deque1.size()) {
    deque1.push_back(deque2[0]);
    deque2.pop_front();
  }
}

int main() {
  int n, a;
  std::deque<int> deque1, deque2;
  char command;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    std::cin >> command;
    if (command == '+') {
      std::cin >> a;
      deque2.push_back(a);
    } else if (command == '*') {
      std::cin >> a;
      deque2.push_front(a);
    } else {
      std::cout << deque1[0] << std::endl;
      deque1.pop_front();
    }
    Balance(deque1, deque2);
  }
}
