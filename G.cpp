#include <cmath>
#include <iomanip>
#include <iostream>

void PrefSums(int* arr, int n) {
  int num;
  std::cin >> num;
  arr[0] = log(num);
  for (int k = 1; k < n; ++k) {
    std::cin >> num;
    arr[k] = arr[k - 1] + log(num);
  }
}

void Process(int* arr, int i, int j) {
  if (i > 0) {
    std::cout << std::setprecision(7) << exp(1. / (j - i + 1) * (arr[j] - arr[i - 1]))
              << std::endl;
  } else {
    std::cout << std::setprecision(7) << exp(1. / (j - i + 1) * arr[j]) << std::endl;
  }
}

int main() {
  int n, q, i, j;
  double num;
  std::cin >> n;
  auto arr = new int[n];
  PrefSums(arr, n);
  std::cin >> q;
  for (int k = 0; k < q; ++k) {
    std::cin >> i >> j;
    Process(arr, i, j);
  }
}
